Feature: Content menu is enabled

  Scenario: User is enabled to see content menu
    As an standard user, I want to get the content menu
    so that I can see the options available and enabled

    Given I get the menu page
    When I select the option Content
    Then I should see the content menu