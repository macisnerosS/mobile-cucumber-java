package steps;

import enums.HomeMenuOption;
import io.cucumber.java.en.When;
import pages.ContentMenuPage;
import pages.HomeMenuPage;
import utils.Cache;

public class WhenStep {
    @When("I select the option {homeMenuOption}")
    public void iSelectTheOption(HomeMenuOption option) {
        HomeMenuPage menuPage = Cache.memory().stateOf(HomeMenuPage.class);

        switch (option) {
            case CONTENT:
                ContentMenuPage contentMenuPage = menuPage.openContent();

                Cache.memory().set(ContentMenuPage.class, contentMenuPage);
                break;
        }
    }
}
