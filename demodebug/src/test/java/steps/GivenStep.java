package steps;

import enums.App;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.cucumber.java.en.Given;
import pages.HomeMenuPage;
import utils.DriverFactory;
import utils.Cache;

public class GivenStep {

    @Given("I get the menu page")
    public void iGetTheMenuPage() {
        HomeMenuPage menuPage = Cache.memory().stateOf(HomeMenuPage.class);

        if (menuPage == null) {
            AppiumDriver driver =
                    DriverFactory.instance().getDriver(App.DEMO_DEBUG);

            menuPage = new HomeMenuPage(driver).open();
        }

        Cache.memory().set(HomeMenuPage.class, menuPage);
    }
}
