package steps;

import enums.App;
import enums.Device;
import enums.HomeMenuOption;
import io.appium.java_client.AppiumDriver;
import io.cucumber.java.BeforeAll;
import io.cucumber.java.ParameterType;
import utils.DriverFactory;

public class Parameter {
    @ParameterType(".*")
    public HomeMenuOption homeMenuOption(String option) {
        return HomeMenuOption.valueOf(option.toUpperCase());
    }

    @BeforeAll
    public static void before_all() {
//        AppiumDriverLocalService service = AppiumDriverLocalService.buildService(
//                new AppiumServiceBuilder()
//                        .usingPort(4723)
//                        .withIPAddress("127.0.0.1")
////                        .withArgument(() -> "--allow-cors")
//        ).withBasePath("/");
//        service.start();
//        service.stop();
    }
}
