package pages;

import io.appium.java_client.AppiumDriver;
import page.Page;

public class HomeMenuPage extends Page {
    public HomeMenuPage(AppiumDriver driver) {
        super(driver);
    }

    public HomeMenuPage open() {
        System.out.println("App launched");

        return this;
    }

    public ContentMenuPage openContent() {
        return new ContentMenuPage(driver);
    }
}
