package page;

import org.openqa.selenium.WebElement;

public abstract class Component {
    protected WebElement root;

    protected Component(WebElement root) {
        this.root = root;
    }
}
