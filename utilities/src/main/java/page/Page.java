package page;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Page implements IPage {
    protected final AppiumDriver driver;

    public Page(AppiumDriver driver) {
        this.driver = driver;
    }

    public WebElement waitForPresenceOf(By locator, Duration duration) {
        WebDriverWait wait = new WebDriverWait(driver, duration);

        try {
            return wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        } catch (Exception ignored) {
        }

        return null;
    }
}
