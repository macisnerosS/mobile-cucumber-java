package enums;

public enum CommandLine {
    USER_DIR,
    PLATFORM_OS;

    public String value() {
        return System.getProperty(
                this.name().toLowerCase()
                        .replace("_", "."));
    }
}
