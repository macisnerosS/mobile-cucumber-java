package utils;

import enums.App;
import enums.CommandLine;
import enums.Device;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobilePlatform;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class DriverFactory {
    final class DriverConfiguration {
        private Device device;
        private App app;

        DriverConfiguration(Device device, App app) {
            this.device = device;
            this.app = app;
        }
    }

    private final String REMOTE_URL = "http://127.0.0.1:4723";
    private final String SWAG_LAB_APP_ANDROID = CommandLine.USER_DIR.value() + "/Android.SauceLabs.Mobile.Sample.app.2.7.1.apk";
    private final String SWAG_LAB_APP_IOS = CommandLine.USER_DIR.value() + "/iOS.Simulator.SauceLabs.Mobile.Sample.app.2.7.1.app";
    private final String DEMO_DEBUG_APP = "ApiDemos-debug.apk";

    private static DriverFactory instance = null;

    private DriverFactory() {
    }

    public static DriverFactory instance() {
        if (instance == null) {
            instance = new DriverFactory();
        }

        return instance;
    }

    public AppiumDriver getDriver(App app) {
        Device device = Device.valueOf(
                CommandLine.PLATFORM_OS.value().toUpperCase());

        DriverConfiguration configuration = new DriverConfiguration(device, app);

        return implementInstanceOf(configuration);
    }

    private DesiredCapabilities getCapabilitiesFor(DriverConfiguration configuration) {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        switch (configuration.device) {
            case ANDROID:
                capabilities.setCapability("appium:deviceName", "emulator-5554");
                capabilities.setCapability("platformName", MobilePlatform.ANDROID);
                capabilities.setCapability("appium:automationName", AutomationName.ANDROID_UIAUTOMATOR2);
                break;
            case IOS:
                // This is the id of the device emulator gotten from xcode
                capabilities.setCapability("appium:udid", "2D9497EE-14FA-4162-95E1-8044FE08C866");
                capabilities.setCapability("platformName", MobilePlatform.IOS);
                capabilities.setCapability("appium:automationName", AutomationName.IOS_XCUI_TEST);
                break;
            default:
                capabilities.setCapability("platformName", MobilePlatform.ANDROID);
                capabilities.setCapability("appium:automationName", AutomationName.ANDROID_UIAUTOMATOR2);
                break;
        }

        if (configuration.device.equals(Device.ANDROID)) {
            switch (configuration.app) {
                case SWAG_LAB:
                    capabilities.setCapability("appium:app", SWAG_LAB_APP_ANDROID);
                    // This capability will wait until main activity and avoid launching error
                    capabilities.setCapability("appium:appWaitActivity", "com.swaglabsmobileapp.MainActivity");
                    break;
                case DEMO_DEBUG:
                    capabilities.setCapability("appium:app", DEMO_DEBUG_APP);
                    break;
            }
        } else if (configuration.device.equals(Device.IOS)) {
            switch (configuration.app) {
                case SWAG_LAB:
                    capabilities.setCapability("appium:app", SWAG_LAB_APP_IOS);
                    break;
                case DEMO_DEBUG:
                    capabilities.setCapability("appium:app", DEMO_DEBUG_APP);
                    break;
            }
        }

        return capabilities;
    }

    private AppiumDriver implementInstanceOf(DriverConfiguration configuration) {
        try {
            DesiredCapabilities capabilities = getCapabilitiesFor(configuration);

            return new AppiumDriver(new URL(REMOTE_URL), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
