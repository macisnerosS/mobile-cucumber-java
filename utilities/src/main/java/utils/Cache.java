package utils;

import page.IPage;
import page.Page;

import java.util.HashMap;
import java.util.Map;

public class Cache {
    private static Cache instance = null;
    private Map<Class<? extends Page>, Page> pages = new HashMap<>();

    private Cache() {
    }

    public static Cache memory() {
        if (instance == null) {
            instance = new Cache();
        }
        return instance;
    }

    public <T extends IPage> void set(Class<T> pageClass, T page) {
        pages.put((Class<? extends Page>) pageClass, (Page) page);
    }

    @SuppressWarnings("unchecked")
    public <T extends IPage> T stateOf(Class<T> pageClass) {
        return (T) pages.get(pageClass);
    }
}
