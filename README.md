#set up the project:

##Install SDK:

###1.- Install android studio https://developer.android.com/studio

###2.- Once installed, open it up to download SDK

###3.- (Optional) Get specific SDK version according to android version of device https://en.wikipedia.org/wiki/Android_version_history using the SDK manager of Android Studio (Tools > SDK Manager)

###4.- (Optional) Uninstall Android Studio

###5.- Verify environment variables ANDROID_HOME and ANDROID_SDK_ROOT are set and storing path of sdk C:\Users\\${NAME_OF_USER}\AppData\Local\Android\Sdk

###6.- Add C:\Users\\${NAME_OF_USER}\AppData\Local\Android\Sdk\platform-tools to system environment variable PATH

##Set appium:

###1.- Install nodeJS: https://nodejs.org/en/

###2.- Install appium:
    
    npm install -g appium

###3.- Optional: Install appium doctor

    npm install -g appium-doctor

###4.- Optional: Execute appium doctor in node.js
    
    appium-doctor

###5.- Start up appium server in terminal

    appium -a 127.0.0.1 --allow-cors

###6.- (optional) Download and start up appium GUI https://appium.io/

###Note: It's not possible to execute appium by terminal and GUI at the same time

##Execute test cases

###1.- Execute appium by either terminal or GUI before running the tests

###2.- Run test cases

###Important note: If appium server is started up from IDE terminal, make sure variables ANDROID_HOME and ANDROID_SDK_ROOT are set in IDE

##Inspect element

###1.- To connect to the device using appium GUI, you have to go to Advance and config appium as:

* Allow CORS=true
* Server Address="127.0.0.1" or "localhost"

###2.- Go to Appium inspector

Note: For latest version inspector has been separated and placed in https://inspector.appiumpro.com/

###3.- Set inspector config as:

* Allow Unauthorized Certificates=true
* Remote Path="/wd/hub"
* Server Address="127.0.0.1" or "localhost"

###4.- Set desired capabilities as:

* deviceName="6JH4C20212001760"
* platformName="Android"
* (optional) app="PATH_OF_APK"
* (optional) appPackage="APP_PACKAGE"
* (optional) appActivity="APP_ACTIVITY"
* (optional) noReset=true

###5.- Click on Start Session

##Get AppPackage and ActivityApp:

###1.- Connect to the device using appium

###2.- Open up the app in device

###3.- Open up a terminal and execute the following:

    adb shell "dumpsys activity activities | grep mResumedActivity"

###4.- Result displayed follows the format:

    mResumedActivity: ActivityRecord{XXXXXXX XX ${PACKAGE_APP}/${ACTIVITY_APP} XXX}

##Multiple devices in adb list devices

When you have to execute command "adb shell" but you have many devices connected
then you have execute "adb -s DEVICE_NAME shell"

##Device is always unauthorized:

###1.- On the device, revoke debugging permission USB

###2.- Disconnect device

###3.- In terminal, execute:

    adb kill-server
    adb start-server

###4.- Reconnect device

###5.- On device, a pop up should be shown and you have to accept it