package steps;

import io.cucumber.java.en.When;
import page.interfaces.ILoginPage;
import page.interfaces.IShoppingPage;
import utils.Cache;

import java.util.List;
import java.util.Map;

public class WhenStep {
    @When("I use the credentials")
    public void iUseTheCredentials(List<Map<String, String>> credentials) {
        Map<String, String> row = credentials.get(0);
        String username = row.get("username");
        String password = row.get("password");

        ILoginPage loadedLoginPage = Cache.memory().stateOf(ILoginPage.class);
        IShoppingPage shoppingPage = loadedLoginPage.login(username, password);

        Cache.memory().set(IShoppingPage.class, shoppingPage);
    }
}
