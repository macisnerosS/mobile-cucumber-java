package steps;

import io.cucumber.java.en.Then;
import org.junit.jupiter.api.Assertions;
import page.interfaces.ILoginPage;
import page.interfaces.IShoppingPage;
import utils.Cache;

public class ThenStep {
    private final String USER_LOCKED_MSG = "Sorry, this user has been locked out.";

    @Then("I should be redirected to shopping page")
    public void iShouldBeRedirectedToShoppingPage() {
        IShoppingPage shoppingPage = Cache.memory().stateOf(IShoppingPage.class);
        IShoppingPage loadedShoppingPage = shoppingPage.load();

        Assertions.assertTrue(loadedShoppingPage.isVisible(),
                "Assert shopping page is visible");
    }

    @Then("I should be notified I am locked out")
    public void iShouldBeNotifiedIAmLockedOut() {
        ILoginPage lockedOutPage = Cache.memory().stateOf(ILoginPage.class);

        Assertions.assertEquals(USER_LOCKED_MSG, lockedOutPage.getLockedOutMessage(),
                "Assert locket out message is displayed");
    }
}
