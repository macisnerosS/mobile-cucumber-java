package steps;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.cucumber.java.AfterAll;
import io.cucumber.java.BeforeAll;
import io.cucumber.java.ParameterType;
import enums.App;
import utils.DriverFactory;

public class Parameter {
    private static AppiumDriverLocalService service;

    @ParameterType(".*")
    public AppiumDriver driver(String appName) {
        return DriverFactory.instance().getDriver(App.valueOf(appName));
    }

    @BeforeAll
    public static void before_all() {
        service = AppiumDriverLocalService.buildService(
                new AppiumServiceBuilder()
                        .usingPort(4723)
                        .withIPAddress("127.0.0.1")
                        .withArgument(() -> "--allow-cors")
                        .withArgument(() -> "--session-override")
        ).withBasePath("/");

        service.start();
    }

    @AfterAll
    public static void after_all() {
        service.stop();
    }
}
