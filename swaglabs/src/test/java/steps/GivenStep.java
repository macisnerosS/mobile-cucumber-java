package steps;

import enums.CommandLine;
import enums.Device;
import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Given;
import page.android.AndroidLoginPage;
import page.ios.IosLoginPage;
import page.interfaces.ILoginPage;
import utils.Cache;

public class GivenStep {
    @Given("I open {driver}")
    public void iOpen(AppiumDriver driver) {
        ILoginPage loginPage = getLoginPage(driver);

        Cache.memory().set(ILoginPage.class, loginPage);
    }

    @Given("I get the login page")
    public void iGetTheLoginPage() {
        ILoginPage loginPage = Cache.memory().stateOf(ILoginPage.class);
        ILoginPage loadedLoginPage = loginPage.load();

        Cache.memory().set(ILoginPage.class, loadedLoginPage);
    }

    private ILoginPage getLoginPage(AppiumDriver driver) {
        String platformOs =
                CommandLine.PLATFORM_OS.value().toUpperCase();

        switch (Device.valueOf(platformOs)) {
            case ANDROID:
                return new AndroidLoginPage(driver).open();
            case IOS:
                return new IosLoginPage(driver).open();
            default:
                throw new IllegalArgumentException("Non identified device os");
        }
    }
}
