Feature: Login - Happy path

  Background:
    Given I open SWAG_LAB

  Scenario: Login succesfully - Standard User
  As a common user, I want to login successfully
  so that I can get home

    Given I get the login page
    When I use the credentials
      | username      | password     |
      | standard_user | secret_sauce |
    Then I should be redirected to shopping page

  Scenario: Login succesfully - Locket Out User
  As a locked out user, I want to log in failed
  so that I can see an error message

    Given I get the login page
    When I use the credentials
      | username        | password     |
      | locked_out_user | secret_sauce |
    Then I should be notified I am locked out

  Scenario: Login successfully - Problem User
  As a problem user, I want to log in successfully but slowly
  so that I can get in the app

    Given I get the login page
    When I use the credentials
      | username     | password     |
      | problem_user | secret_sauce |
    Then I should be redirected to shopping page