package page.android;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import page.Page;
import page.interfaces.IShoppingPage;

import java.time.Duration;


public class AndroidShoppingPage extends Page implements IShoppingPage {
    private final By VIEW_PRODUCTS = By.xpath("//android.widget.ScrollView[@content-desc='test-PRODUCTS']");

    public AndroidShoppingPage(AppiumDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return !driver.findElements(VIEW_PRODUCTS).isEmpty();
    }

    @Override
    public AndroidShoppingPage load() {
        waitForPresenceOf(VIEW_PRODUCTS, Duration.ofSeconds(5));

        return this;
    }
}
