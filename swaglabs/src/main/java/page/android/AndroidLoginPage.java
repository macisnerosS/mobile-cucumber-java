package page.android;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.Page;
import page.android.component.AndroidInnerMessageComponent;
import page.interfaces.ILoginPage;

import java.time.Duration;

public class AndroidLoginPage extends Page implements ILoginPage {
    private final By TXT_USERNAME =
            By.xpath("//android.widget.EditText[@content-desc='test-Username']");
    private final By TXT_PASSWORD =
            By.xpath("//android.widget.EditText[@content-desc='test-Password']");
    private final By BTN_SUBMIT =
            By.xpath("//android.view.ViewGroup[@content-desc='test-LOGIN']");
    private final By LBL_ERROR_MESSAGE =
            By.xpath("//android.view.ViewGroup[@content-desc='test-Error message']");

    public AndroidLoginPage(AppiumDriver driver) {
        super(driver);
    }

    public AndroidLoginPage open() {
        System.out.println("Login page opened");

        return this;
    }

    public AndroidShoppingPage login(String username, String password) {
        driver.findElement(TXT_USERNAME).sendKeys(username);
        driver.findElement(TXT_PASSWORD).sendKeys(password);
        driver.findElement(BTN_SUBMIT).click();

        return new AndroidShoppingPage(driver);
    }

    public AndroidLoginPage load() {
        try {
            WebDriverWait wait =
                    new WebDriverWait(driver, Duration.ofSeconds(5));
            wait.until(
                    ExpectedConditions.presenceOfElementLocated(TXT_USERNAME));
        } catch (NotFoundException ignored) {
        }

        return this;
    }

    public String getLockedOutMessage() {
        AndroidInnerMessageComponent innerMessage =
                new AndroidInnerMessageComponent(driver.findElement(LBL_ERROR_MESSAGE));

        return innerMessage.value();
    }
}
