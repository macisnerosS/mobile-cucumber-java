package page.android.component;

import page.Component;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class AndroidInnerMessageComponent extends Component {
    private By message = By.xpath("//android.widget.TextView");

    public AndroidInnerMessageComponent(WebElement root) {
        super(root);
    }

    public String value() {
        return root.findElement(message).getText().trim();
    }
}
