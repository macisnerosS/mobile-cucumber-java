package page.ios;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.Page;
import page.interfaces.ILoginPage;

import java.time.Duration;

public class IosLoginPage extends Page implements ILoginPage {
    private final By TXT_USERNAME =
            By.xpath("//android.widget.EditText[@content-desc='test-Username']");
    private final By TXT_PASSWORD =
            By.xpath("//android.widget.EditText[@content-desc='test-Password']");
    private final By BTN_SUBMIT =
            By.xpath("//android.view.ViewGroup[@content-desc='test-LOGIN']");
    private final By LBL_SORRY_USER_LOCKED =
            By.xpath("//android.widget.TextView[@text='Lo sentimos, este usuario ha sido bloqueado.']");

    public IosLoginPage(AppiumDriver driver) {
        super(driver);
    }

    public IosLoginPage open() {
        System.out.println("Login page opened");

        return this;
    }

    public IosShoppingPage login(String username, String password) {
        driver.findElement(TXT_USERNAME).sendKeys(username);
        driver.findElement(TXT_PASSWORD).sendKeys(password);
        driver.findElement(BTN_SUBMIT).click();

        return new IosShoppingPage(driver);
    }

    public IosLoginPage load() {
        try {
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
            wait.until(ExpectedConditions.presenceOfElementLocated(TXT_USERNAME));
        } catch (NotFoundException ignored) {
        }

        return this;
    }

    public String getLockedOutMessage() {
        return driver.findElement(LBL_SORRY_USER_LOCKED).getText();
    }
}
