package page.ios;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import page.Page;
import page.interfaces.IShoppingPage;

public class IosShoppingPage extends Page implements IShoppingPage {
    private final By VIEW_PRODUCTS = By.xpath("//android.widget.ScrollView[@content-desc='test-PRODUCTS']");

    public IosShoppingPage(AppiumDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return !driver.findElements(VIEW_PRODUCTS).isEmpty();
    }

    @Override
    public IosShoppingPage load() {
        return null;
    }
}
