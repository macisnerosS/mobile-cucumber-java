package page.interfaces;

import page.IPage;

public interface ILoginPage extends IPage {
    String getLockedOutMessage();
    ILoginPage load();
    IShoppingPage login(String username, String password);
}
