package page.interfaces;

import page.IPage;

public interface IShoppingPage extends IPage {
    boolean isVisible();
    IShoppingPage load();
}
